#pragma once

#ifndef STARFIELD_H
#define STARFIELD_H

struct Star
{
	float xPos;
	float yPos;
	float zPos;
};

///screen saver class - this is responsible for rendering the screensaver- it has all the OpenGL initialization code as well
class CScreenSaver
{
public:
	void RenderScreenSaver();
	void Prepare(float dt);

	void SetupProjection(int width, int height);

	bool ShutDown();
	bool InitGraphics();

	float GetAngle(){return m_angle;}
	int GetWindowHeight(){return m_windowHeight;}
	int GetWindowWidth(){return m_windowWidth;}
	int GetWindowBits(){return m_windowBits;}
	Star* GetStarArray(){return m_stars;}

	void UpdateStarPosition();	//update the star positions per frame
	void DrawStars();	//renders the stars to the screen ..rendered as small rectangles

	CScreenSaver(void);
	virtual ~CScreenSaver(void);

private:
	float m_angle;
	float m_zTranslation;
	int m_windowHeight;
	int m_windowWidth;
	int m_windowBits;
	Star* m_stars;

	void InitializeStars();	//initializes the stars to a random position according to the given equation
};

#endif