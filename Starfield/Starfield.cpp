//graphics and the main rendering code goes here

#include <windows.h>
#include <iostream>
#include <cstdio>
#include <time.h>
#include <math.h>
#include <stdlib.h>
#include <gl\gl.h>
#include <gl\glu.h>
#include <gl\glaux.h>

#include "Starfield.h"

using namespace std;

CScreenSaver::CScreenSaver(void)
{
	m_windowBits = 24;
	m_windowHeight = 768;
	m_windowWidth = 1024;
	m_angle = 0.0;
	m_zTranslation = -8.0f;
	m_stars = new Star[500];
	InitializeStars();
}

CScreenSaver::~CScreenSaver(void)
{
}

bool CScreenSaver::InitGraphics()
{
	glEnable(GL_TEXTURE_2D);
	glShadeModel(GL_SMOOTH);
	glEnable(GL_DEPTH_TEST);

	return true;
}

bool CScreenSaver::ShutDown()
{
	return true;
}

void CScreenSaver::SetupProjection(int width, int height)
{
	if(height == 0)
	{
		height = 1;
	}

	glViewport(0,0,width,height); // reset the viewport

	// Select the Projection Matrix
	glMatrixMode(GL_PROJECTION);
	// reset the matrix
	glLoadIdentity();
	
		// Calculate The Aspect Ratio Of The Window
	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);

	// set the Modelview matrix to load the objects
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	m_windowHeight = height;
	m_windowWidth = width;
}

void CScreenSaver::Prepare(float dt)
{
//	m_zTranslation = m_zTranslation - (0.002 * dt);

	UpdateStarPosition();
}

void CScreenSaver::RenderScreenSaver()
{
	glClearColor(0.0f,0.0f,0.0f,0.0f);
	// clear the screen and the depth buffer 
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glPushMatrix();
		glLoadIdentity();
		glTranslatef(0.0f,0.0f,m_zTranslation);
		glColor3f(1.0f,0.0f,0.0f);
		DrawStars();
	glPopMatrix();

	//glColor3f(1.0f,1.0f,1.0f);
}

void CScreenSaver::InitializeStars()
{
	for(int i = 0; i < 500; i++)
	{
		m_stars[i].xPos = (rand() % 6) * 0.2 * (rand() % 20) * pow((-1.0),rand()%5);
		m_stars[i].yPos = (rand() % 6) * 0.2 * (rand() % 20) * pow((-1.0),rand()%5);
		m_stars[i].zPos = (-1.0) * (rand() % 50);
	}
}

void CScreenSaver::UpdateStarPosition()
{
	for(int i = 0; i < 500; i++)
	{
		m_stars[i].xPos += 0.1;
		m_stars[i].yPos += 0.1;
		if(m_stars[i].zPos > -50)
			m_stars[i].zPos++;
		else 
			m_stars[i].zPos--;

		if(m_stars[i].zPos > 6.0)
		{
			m_stars[i].xPos = (rand() % 6) * 0.2 * (rand() % 20) * pow((-1.0),rand()%5);
			m_stars[i].yPos = (rand() % 6) * 0.2 * (rand() % 20) * pow((-1.0),rand()%5);
			m_stars[i].zPos = (-1.0) * (rand() % 50);
		}



	}
}

void CScreenSaver::DrawStars()
{
	for(int i = 0; i < 500; i++)
	{
		glPushMatrix();
			glColor3f(1.0f,1.0f,1.0f);
			glTranslatef(m_stars[i].xPos,m_stars[i].yPos,m_stars[i].zPos);
			glScalef(0.005f,0.005f,0.005f);
			glBegin(GL_QUADS);
				glVertex3f(-1.0f,1.0f,0.0f);
				glVertex3f(1.0f,1.0f,0.0f);
				glVertex3f(1.0f,-1.0f,0.0f);
				glVertex3f(-1.0f,-1.0f,0.0f);
			glEnd();
		glPopMatrix();
	}
}