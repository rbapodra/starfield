//Main.cpp for our screensaver application

#include <Windows.h>
#include <gl\gl.h>
#include <gl\glu.h>
#include <gl\glaux.h>
#include <math.h>

#include "Starfield.h"
#include "timer.h"

LRESULT CALLBACK WndProc(HWND hwnd,UINT message,WPARAM wParam,LPARAM lParam);
void OpenGLSetup(HDC hDC);

HDC hDC;

CScreenSaver starField;
CHiResTimer *g_hiResTimer = NULL;

int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpCmdLine,int nCmdShow)
{
	 // Define the main window class
	WNDCLASSEX wcex;
	HWND hwnd;


	MSG msg;
	bool quit = false;
	
	g_hiResTimer = new CHiResTimer;

	RECT windowRect;
	DWORD dwExStyle;
	DWORD dwStyle;
	
	windowRect.left = (long)0;
	windowRect.right = (long)1024;
	windowRect.top = (long)0;
	windowRect.bottom = (long)768;

	wcex.cbSize			=	sizeof(WNDCLASSEX) ;
	wcex.style			=   CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
	wcex.cbClsExtra		=   0 ;
	wcex.cbWndExtra		=   0 ;
	wcex.lpfnWndProc	=   WndProc ;
	wcex.hInstance		=   hInstance ;
	wcex.hIcon			=   LoadIcon(NULL,IDI_APPLICATION);
	wcex.hCursor		=   LoadCursor(NULL,IDC_ARROW) ;
	wcex.hbrBackground	=	NULL;		
	wcex.hIconSm		=   LoadIcon(NULL,IDI_APPLICATION);
	wcex.lpszMenuName	=	NULL ;
	wcex.lpszClassName	=	"Starfield ScreenSaver" ;	
	
	if(!RegisterClassEx(&wcex))
		return 0;
	
	// Once window is registered, before displaying check if fullscreen is enabled or not
	

		dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;	
		dwStyle = WS_OVERLAPPEDWINDOW;

	//dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;

	AdjustWindowRectEx(&windowRect,dwStyle,FALSE,dwExStyle);
	// Create the Window

	hwnd = CreateWindowEx(0,
						  "Starfield ScreenSaver",
						  "Starfield",
						  dwStyle |
						  WS_CLIPCHILDREN |
						  WS_CLIPSIBLINGS,
						  0,0,	// checkpoint
						  windowRect.right - windowRect.left,
						  windowRect.bottom - windowRect.top,
						  NULL,
						  NULL,
						  hInstance,
						  NULL
						  );

	hDC = GetDC(hwnd);

	if(hwnd == NULL)
		return 0;
	
		
	ShowWindow(hwnd,SW_SHOW);
	UpdateWindow(hwnd);

	if(!starField.InitGraphics())
	{
		MessageBox(NULL,"ScreenSaver::InitGraphics() error!","ScreenSaver class failed to initialize!", MB_OK);
		return -1;
	}

	while(1)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
			if(msg.message == WM_QUIT)
			{
				break;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			starField.Prepare(g_hiResTimer->GetElapsedSeconds(1));
			starField.RenderScreenSaver();
			SwapBuffers(hDC);
		
		}
	}

 ReleaseDC(hwnd,hDC);

 return msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd,UINT message,WPARAM wParam,LPARAM lParam)
{
	static HDC hDC;
 	static HGLRC hRC;
	int width,height;

	switch(message)
	{
		case WM_CREATE:
			hDC = GetDC(hwnd);
			OpenGLSetup(hDC);
			hRC = wglCreateContext(hDC);	// Once pixel format is set up create the RC
			wglMakeCurrent(hDC,hRC);
			break;

		case WM_CLOSE:
		case WM_DESTROY:
			wglMakeCurrent(hDC,NULL);
			wglDeleteContext(hRC);
			PostQuitMessage(0);
			break;

		case WM_SIZE:
			height = HIWORD(lParam);
			width = LOWORD(lParam);
			starField.SetupProjection(width,height);
			break;

		case WM_PAINT:
			PAINTSTRUCT ps;
			BeginPaint(hwnd,&ps);
			EndPaint(hwnd,&ps);
			break;

		case WM_KEYDOWN:
			int fwKeys;
			LPARAM keyData;
			fwKeys = (int)wParam;
			keyData = lParam;
			switch(fwKeys)
			{
			case VK_ESCAPE:
				PostQuitMessage(0);
				break;
			default:
				break;
			}
			break;

		default: return DefWindowProc(hwnd,message,wParam,lParam);

	}

	return 0;

}

void OpenGLSetup(HDC hDC)
{
	GLuint iPixelFormat;

	PIXELFORMATDESCRIPTOR pfd = 
	{
		sizeof(PIXELFORMATDESCRIPTOR),
		1,
		PFD_SUPPORT_OPENGL | 
		PFD_DRAW_TO_WINDOW |
		PFD_DOUBLEBUFFER,
		PFD_TYPE_RGBA,
		32,
		0,0,0,0,0,0,
		0,
		0,
		0,
		0,0,0,0,
		16,
		0,
		0,
		PFD_MAIN_PLANE,
		0,
		0,0,0
	};
	
	iPixelFormat = ChoosePixelFormat(hDC,&pfd); // int or GLUint doesnt matter
	SetPixelFormat(hDC,iPixelFormat,&pfd);
	
}